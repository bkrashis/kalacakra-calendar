math = require('mathjs');
const tr = require(process.env.KALACAKRA_TRADITION||'../kalacakra/tsurphu-general.js');
util = require('../kalacakra/lib/util.js')

const weekdays = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
const months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];


function day_calendar(tr, day_result, is_lhag) {
  var date = day_result.spizag().date;
  console.log(day_result.day()+":", weekdays[date.getDay()]+".",
                      day_result.lunar_mansion_cycles().mansion('sa_ascii')+"/"+day_result.lunar_mansion_cycles().mansion('bo_wylie')+".",
                      day_result.solar_day_cycles().weekday_four_element('en')+"-"+day_result.lunar_mansion_cycles().element('en')+";",
                      date.getDate(),
                      months[date.getMonth()],
                      date.getFullYear()
                    );

  var gzadag = "";

  if (is_lhag) {
    // The previous gzadag was just before a whole number, and this one is just after a whole number.
    // For the lhag day we use a gza-dag of today's whole number,
    // but Henning represents this conventionally as (n-1);60,0 rather than as (n);0,0
    gzadag = ((day_result.gza().gzadag+6)%7)+";60,0";
  } else {
    console.log(" ", day_result.sbyorba().name('sa_ascii')+"/"+day_result.sbyorba().name('bo_wylie')+",",
                     day_result.byedpa().name('sa_ascii')+"/"+day_result.byedpa().name('bo_wylie')+",",
                       day_result.lunar_day_cycles().animal('en')+",",
                       day_result.lunar_day_cycles().trigram('bo_wylie'),
                       day_result.lunar_day_cycles().smeba
                     );

    gzadag = util.str_henning(day_result.gza().gzadag, [60, 60], false, false, 2);
  }

  console.log("  Solar:", day_result.solar_day_cycles().element('en')+"-"+day_result.solar_day_cycles().animal('en')+".",
                    day_result.solar_day_cycles().chinese_lunar_mansion('zh_pinyin'));

  console.log(" ", gzadag,
                     util.str_henning(day_result.monsar().monsar, [60, 60], false, false, 2),
                     util.str_henning(day_result.nyi().nyidag.longitude, [60, 60], false, false, 2),
                     util.str_henning(day_result.sbyorba().value, [60, 60], false, false, 2),
                     util.str_henning(day_result.nyi().nyidag.sign, [30, 60], false, false, 2));

  var ee = tr.events.func(day_result);
  for (var e in ee) {
    if (ee[e]) {
      console.log("  "+tr.events[e].en+".");
    }
  }
  var ss = tr.sadag.func(day_result);
  var sout = "";
  var sep = "    ";
  for (var s in ss) {
    if (ss[s]) {
      sout += sep+tr.sadag[s].bo_wylie;
      sep = ", ";
    }
  }
  if (sout) console.log(sout);
}

function month_calendar(tr, month_result) {
  if (month_result[1].zla().month==1) {
    // FIXME: in 2019 phugpa has an intercalary first month so this happens twice
    console.log("\nNew Year:", y+",",
          month_result[1].year_cycles().element('en') + "-"
          + month_result[1].year_cycles().gender('en') + "-"
          + month_result[1].year_cycles().animal('en'));
  }
  var ic = "";
  if (month_result[1].zla().intercalary) {
    ic = " (Intercalary)"
  }
  console.log("\nTibetan Lunar Month:",month_result[1].zla().month+ic,"-",
          month_result[1].month_cycles().element('en') + "-"
          + month_result[1].month_cycles().gender('en') + "-"
          + month_result[1].month_cycles().animal('en'));

  console.log("\nMonth:",util.str_henning(month_result[1].zla().zladag, [65], false, false, 1));
  console.log("Anomaly:",util.str_henning(month_result[1].rilcha(), [126], false, false, 1));
  console.log("Mean Weekday:",util.str_henning(month_result[1].gza().gzadru, [60, 60, 6, 707], false, false, 4));
  console.log("Mean Sun:",util.str_henning(month_result[1].nyi().nyidru, [60, 60, 6, 13], false, false, 4));
  console.log();

  for (d=0; d<month_result.length; d++) {
    //day_as_henning(month_result[d]);
    if (month_result[d].chad()) {
      console.log(month_result[d].day()+". Omitted.");
      console.log(" ", util.str_henning(month_result[d].gza().gzadag, [60, 60], false, false, 2),
                       util.str_henning(month_result[d].nyi().nyidag.longitude, [60, 60], false, false, 2));
    } else {
      if (month_result[d].lhag()) {
        day_calendar(tr, month_result[d].lhag(), true);
      }
      day_calendar(tr, month_result[d])
    }
  }

}

var y = new Date().getFullYear();

for (m=1; m<=12; m++) {
    mr = []
    for (d=1; d<=30; d++) {
       result = tr.calculate(y, m, d, false);
       mr.push(result);
    }
    month_calendar(tr, mr);
    // If the month correction has been active, then we need to rerun this month with the flag to catch up.
    if (mr[mr.length-1].zla().zeromthfg) {
      mr = []
      for (d=1; d<=30; d++) {
         result = tr.calculate(y, m, d, true);
         mr.push(result);
      }
      month_calendar(tr, mr);
    }
}
