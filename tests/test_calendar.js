const tr = require(process.env.KALACAKRA_TRADITION||'../kalacakra/phugpa.js');
util = require('../kalacakra/lib/util.js')

const weekdays = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
const months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

function day_calendar(tr, day_result, is_lhag) {
  var date = day_result.spizag().date;
  console.log(day_result.day()+":", weekdays[date.getDay()]+".", day_result.lunar_mansion_cycles().lunar_mansion('bo_wylie')+".",
                      day_result.solar_day_cycles().weekday_four_element('en')+"-"+day_result.lunar_mansion_cycles().element('en')+";",
                      date.getDate(),
                      months[date.getMonth()],
                      date.getFullYear()
                    );

  var gzadag = "";
  if (is_lhag) {
    // The previous gzadag was just before a whole number, and this one is just after a whole number.
    // For the lhag day we use a gzadag of today's whole number,
    // but Henning represents this conventionally as (n-1);60,0 rather than as (n);0,0
    gzadag = ((day_result.gza().gzadag+6)%7)+";60,0";
  } else {
    console.log(" ", day_result.sbyorba().name('bo_wylie')+",",
                       day_result.byedpa().name('bo_wylie')+",",
                       day_result.lunar_day_cycles().animal('en')+",",
                       day_result.lunar_day_cycles().trigram('bo_wylie'),
                       day_result.lunar_day_cycles().smeba
                     );

    gzadag = util.str_henning(day_result.gza().gzadag, [60, 60], false, false, 2);
  }

  console.log(" ", gzadag,
                     util.str_henning(day_result.monsar().monsar, [60, 60], false, false, 2),
                     util.str_henning(day_result.nyi().nyidag.longitude, [60, 60], false, false, 2),
                     util.str_henning(day_result.sbyorba().value, [60, 60, 6], false, false, 2),
                     util.str_henning(day_result.nyi().nyibar.sign, [30, 60], false, false, 2));

  console.log("  Solar:", day_result.solar_day_cycles().element('en')+"-"+day_result.solar_day_cycles().animal('en')+".",
                    day_result.solar_day_cycles().chinese_lunar_mansion('zh_pinyin'),
                    day_result.solar_day_cycles().smeba);

  var ee = tr.events.func(day_result);
  for (var e in ee) {
    if (ee[e]) {
      console.log("  "+tr.events[e].en+".");
    }
  }/* sadag aren't mentioned in the comparison file
  var ss = tr.sadag.func(day_result);
  var sout = "";
  var sep = "    ";
  for (var s in ss) {
    if (ss[s]) {
      sout += sep+tr.sadag[s].bo_wylie;
      sep = ", ";
    }
  }
  if (sout) console.log(sout);*/
}

function month_calendar(tr, month_result) {
  if (month_result[1].zla().month==1) {
    // FIXME: in 2019 phugpa has an intercalary first month so this happens twice
    console.log(month_result[1]);
    console.log("\nNew Year:", y+",",
          month_result[1].year_cycles().element('en') + "-"
          + month_result[1].year_cycles().gender('en') + "-"
          + month_result[1].year_cycles().animal('en'));
  }
  var ic = "";
  ic = month_result[1].zla().month;
  if (month_result[1].zla().intercalary) {
    ic += " (Intercalary)";
  }

  console.log("\nTibetan Lunar Month:",ic,"-",
          month_result[1].month_cycles().element('en') + "-"
          + month_result[1].month_cycles().gender('en') + "-"
          + month_result[1].month_cycles().animal('en') + "\n");

  for (d=0; d<month_result.length; d++) {
    //day_as_henning(month_result[d]);
    if (month_result[d].chad()) {
      console.log(month_result[d].day()+". Omitted:",
              month_result[d].lunar_day_cycles().animal('en'),
              month_result[d].lunar_day_cycles().trigram('bo_wylie'),
              month_result[d].lunar_day_cycles().smeba
      );
    } else {
      if (month_result[d].lhag()) {
        day_calendar(tr, month_result[d].lhag(), true);
      }
      day_calendar(tr, month_result[d])
    }
  }

}

for (y=(process.env.KALACAKRA_TEST_FROM_YEAR||2019); y<=(process.env.KALACAKRA_TEST_TO_YEAR||2019); y++) {

    for (m=1; m<=12; m++) {
        console.log("--- Month",y,m);
        mr = []
        for (d=1; d<=30; d++) {
           result = tr.calculate(y, m, d, false);
           mr.push(result);
        }
        month_calendar(tr, mr);
        // If the month correction has been active, then we need to rerun this month with the flag to catch up.
        if (mr[mr.length-1].zla().zeromthfg) {
          console.log("--- Repeating month",y,m);
          mr = []
          for (d=1; d<=30; d++) {
             result = tr.calculate(y, m, d, true);
             mr.push(result);
          }
          month_calendar(tr, mr);
        }
    }

}
