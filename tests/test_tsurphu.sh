#!/bin/bash

export KALACAKRA_TRADITION=../kalacakra/tsurphu-tantra.js
export KALACAKRA_TRADITION_NUMBER=2 # number of the menu option, not required :)

node test_tsurphu.js > a
cat compare/ts_2019.txt | grep -v "slow:" > b # planets not done
diff --minimal --suppress-common-lines --side-by-side a b
#diff --minimal --side-by-side a b 

