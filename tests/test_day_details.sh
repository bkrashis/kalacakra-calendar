#!/bin/bash

# Daybreak -- EH program give unlikely values for daybreak (negative inner radix)
# sgos zhag -- planets, not yet supported

export KALACAKRA_TRADITION=../kalacakra/phugpa.js
export KALACAKRA_TRADITION_NUMBER=1 # number of the menu option, not required :)

node test_day_details.js    | grep -v "Daybreak" > a
cat compare/day_details_pl_2019.txt | grep -v "Daybreak" | grep -v "sgos zhag" > b
#diff --side-by-side a b
diff --suppress-common-lines --side-by-side a b

# For tradition 2 (Generalised Tsurphu) see test_tsurphu_day_details.sh

export KALACAKRA_TRADITION=../kalacakra/khenying.js
export KALACAKRA_TRADITION_NUMBER=4 # number of the menu option, not required :)

node test_day_details.js    | grep -v "Daybreak" > a
cat compare/day_details_khenying_2019.txt | grep -v "Daybreak" | grep -v "sgos zhag" > b
# diff --side-by-side a b
diff --suppress-common-lines --side-by-side a b

export KALACAKRA_TRADITION=../kalacakra/genden.js
export KALACAKRA_TRADITION_NUMBER=5 # number of the menu option, not required :)

node test_day_details.js    | grep -v "Daybreak" > a
cat compare/day_details_genden_2019.txt | grep -v "Daybreak" | grep -v "sgos zhag" > b
# diff --side-by-side a b
diff --suppress-common-lines --side-by-side a b

