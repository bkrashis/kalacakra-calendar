const math = require('mathjs');
const phugpa = require('../kalacakra/phugpa.js');
const util = require('../kalacakra/util.js')

// FIXME this is way old and no longer works with the traditions

function test_worked_example() {
  console.log(tradition.phugpa);

  r = calculate(tradition.phugpa, 4,4,6);

  console.log('zladag = ', util.str_henning(r.work.zladag, [130]),          r.work.zladag.equals(unradix([[53,1],[78,130]]))  );
  console.log('gzadru = ', util.str_henning(r.work.gzadru, [60, 60, 6]),    r.work.gzadru.equals(unradix([[6,1],[37,60],[10,60]]))  );
  console.log('rilcha = ', util.str_henning(r.rilcha, [126]),          r.rilcha.equals(unradix([[0,1],[39,126]]))  );
  console.log('nyidru = ', util.str_henning(r.work.nyidru, [60, 60, 6]),    r.work.nyidru.equals(unradix([[7,1],[39,60],[38,60],[2,6],[10,13]]))  );
  console.log('tsebar = ', util.str_henning(r.tsebar, [60, 60, 6]),    r.tsebar.equals(unradix([[5,1],[31,60],[32,60],[0,6]]))  );
  console.log('gzatot = ', util.str_henning(r.work.gzatot, [60, 60, 6]),    r.work.gzatot.equals(unradix([[0,1],[0,60],[18,60],[3,6]]))  );
  console.log('gzawor = ', util.str_henning(r.work.gzawor, [60, 60, 6]),    r.work.gzawor.equals(unradix([[5,1],[55,60],[50,60],[3,6]]))  );
  console.log('nyilon = ', util.str_henning(r.work.nyilon, [60, 60, 6]),    r.work.nyilon.equals(unradix([[0,1],[4,60],[21,60],[5,6],[9,13]]).mul(6))  );
  console.log('nyibar = ', util.str_henning(r.nyibar, [60, 60, 6]),    r.nyibar.equals(unradix([[8,1],[5,60],[50,60],[0,6],[12,13]]))  );
  console.log('nyidor = ', r.work.nyiwork.dor,                              r.work.nyiwork.dor==false  );
  console.log('nyiwor = ', util.str_henning(r.work.nyiwork.wor, [60, 60, 6]),    r.work.nyiwork.wor.equals(unradix([[1,1],[20,60],[50,60],[0,6],[12,13]]))  );
  console.log('nyitrem = ', r.work.nyiwork.trem,                            r.work.nyiwork.trem==80  );
  console.log('nyitquo = ', r.work.nyiwork.tquo,                            r.work.nyiwork.tquo==0  );
  console.log('nyilista = ', util.str_henning(r.work.nyiwork.lista, [60, 60, 6]),      r.work.nyiwork.lista.equals(unradix([[0,1],[3,60],[35,60],[3,6],[4,13]]))  );
  console.log('nyilistb = ', util.str_henning(r.work.nyiwork.listb, [60, 60, 6]),      r.work.nyiwork.listb.equals(0)  );
  console.log('nyidag = ', util.str_henning(r.nyidag, [60, 60, 6]),         r.nyidag.equals(unradix([[8,1],[2,60],[14,60],[3,6],[8,13]]))  );
  console.log('gzadag = ', util.str_henning(r.gzadag, [60, 60, 6]),         r.gzadag.equals(unradix([[5,1],[52,60],[14,60],[5,6],[9,13]]))  );
  console.log('mondag = ', util.str_henning(r.mondag, [60, 60, 6]),    r.mondag.equals(unradix([[13,1],[26,60],[14,60],[3,6],[8,13]])) );// page says 7, but must be equal to nyidag
  console.log('monsar = ', util.str_henning(r.monsar, [60, 60, 6]),         r.monsar.equals(unradix([[12,1],[33,60],[59,60],[3,6],[12,13]]))  );

  delete r.work;
  console.log();
  console.log(r);
}
