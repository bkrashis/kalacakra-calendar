const math = require('mathjs');
const tr = require(process.env.KALACAKRA_TRADITION||'../kalacakra/phugpa.js');
const util = require('../kalacakra/lib/util.js')

const weekdays = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
const months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Dec" ];

function day_details(r) {
  var henning_month = r.zla().month;
  if (r.zla().intercalary) {
    henning_month = -henning_month;
  }

  console.log("\n\n"+process.env.KALACAKRA_TRADITION_NUMBER+".",tr.label)
  console.log();
  console.log("True month:", util.str_henning(r.zla().zladag, [65], false, false, 1)+", Month =", r.unadjusted_month()+", Year =", r.year());
  console.log("Target date:", r.day(), "/", r.unadjusted_month(), "/", r.year(), "- Corrected Month =", henning_month+",", r.month_cycles().month('bo_wylie'));
  // console.log('+ nyi_dag: nyilon =', util.str_henning(r.nyi().nyilon.longitude, [60, 60, 6], false, true, 4));
  // console.log('+ nyi_dag: nyidru =', util.str_henning(r.nyi().nyidru, [60, 60, 6], false, true, 4));
  console.log('+ nyi_dag: nyiwor =', util.str_henning(r.nyi()._debug.wor, [60, 60, 6], false, true, 4));
  // == list1 console.log('+ nyi_dag: correction =', util.str_henning(r.nyi().correction, [60, 60, 6], false, true, 4));
  console.log('+ nyi_dag: trem =', r.nyi()._debug.trem*1);
  console.log('+ nyi_dag: tquo =', r.nyi()._debug.tquo*1);
  console.log('+ nyi_dag: nyibye[tquo-1] =', math.abs(tr.nyi.bye[r.nyi()._debug.tquo]));
  console.log('+ nyi_dag: lista =', util.str_henning(r.nyi()._debug.lista, [60, 60, 6], false, true, 4));
  console.log('+ nyi_dag: listb =', util.str_henning(r.nyi()._debug.listb, [60, 60, 6], false, true, 4));
  console.log('+ nyi_dag: listc =', util.str_henning(r.nyi().correction, [60, 60, 6], false, true, 4));
  // console.log('+ gza_dag: gzadru =', util.str_henning(r.gzadru, [60, 60, 6], false, true, 4, true));
  // console.log('+ gza_dag: tsedru =', util.str_henning(r.gza()._debug.tsedru, [60, 60, 6], false, true, 4, true));
  console.log('+ gza_dag: tsebar =', util.str_henning(r.gza().tsebar, [60, 60, 6, 707], false, true, 4));
  console.log('+ gza_dag: rilpo =', r.gza()._debug.rilpo*1);
  console.log('+ gza_dag: gzabye[trem-1] =', tr.gza.bye[r.gza()._debug.trem]);
  console.log('+ gza_dag: radj =', r.gza()._debug.radj*1);
  console.log('+ gza_dag: tot =', r.gza()._debug.list1.mul(60).mul(60).mul(6).mul(707)*1);
  console.log('+ gza_dag: list1 =', util.str_henning(r.gza()._debug.list1, [60, 60, 6, 707], false, true, 4));
  console.log('+ gza_dag: list2 =', util.str_henning(r.gza()._debug.list2, [60, 60, 6, 707], false, true, 4));
  console.log('+ gza_dag: list3 =', util.str_henning(r.gza()._debug.list3, [60, 60, 6, 707], false, true, 4));
  console.log('+ gza_dag: gzawor =', util.str_henning(r.gza().gzawor, [60, 60, 6], false, true, 4, true));

  console.log('Rahu month:', r.rahu()[15].month+', gdong, 15th:', util.str_henning(r.rahu()[15].gdong, [60, 60, 6], false, false, 4), '- 30th:', util.str_henning(r.rahu()[30].gdong, [60, 60, 6], false, false, 4));
  console.log("ril cha =", util.str_henning(r.rilcha(), [126], false, false, 1));
  console.log("The gza-bar is:", util.str_henning(r.gza().tsebar, [60, 60, 6, 707], false, true, 4));
  console.log("The nyi-bar is:", util.str_henning(r.nyi().nyibar.longitude, [60, 60, 6], false, true, 5), "-", (r.nyi().nyibar.degrees).toFixed(6));
  console.log("The gza-dag is:", util.str_henning(r.gza().gzadag, [60, 60, 6, 707], false, true, 4));
  console.log("The nyi-dag is:", util.str_henning(r.nyi().nyidag.longitude, [60, 60, 6], false, true, 5), "-", (r.nyi().nyidag.degrees).toFixed(6));
  // Henning's code gives unlikely numbers for the daybreak value (negative fractional parts)
  console.log("Moon, lunar day:", util.str_henning(r.monsar().mondag, [60, 60, 6], false, false, 5), "- Daybreak:", util.str_henning(r.monsar().monsar, [60, 60, 6], false, false, 5)+".");
  console.log("General day factors:", r.spizag().frac_b*1+",", r.spizag().frac_c*1);
  // Nyindhru is reported to 5 places but the last is always reported as 0, probably some algorithmic corner cutting.
  console.log("Dragkang factor:",r.dragkang().dragkres*1,"- Nyindhru:", util.str_henning(r.nyindhru(), [60,60,6,tr.nyindhru.frac], false, false, 4)+",0");
/* TODO: planets
  console.log("Mars sgos zhag =", r.mars.gozhag+":", util.str_henning(r.mars.daldag, [60, 60, 6, tr.mars.frac], false, false, 4));
  console.log("Jupiter sgos zhag =", r.jupiter.gozhag+":", util.str_henning(r.jupiter.daldag, [60, 60, 6, tr.jupiter.frac], false, false, 4));
  console.log("Saturn sgos zhag =", r.saturn.gozhag+":", util.str_henning(r.saturn.daldag, [60, 60, 6, tr.saturn.frac], false, false, 4));
  console.log("Mercury sgos zhag =", r.mercury.gozhag+":", util.str_henning(r.mercury.kanbar, [60, 60, 6, tr.mercury.frac], false, false, 4));
  console.log("Venus sgos zhag =", r.venus.gozhag+":", util.str_henning(r.venus.kanbar, [60, 60, 6, tr.venus.frac], false, false, 4));
  */
  console.log("SPYI ZHAG =", r.spizag().spizag+". Julian day =", r.spizag().juldat+". Western date =",weekdays[r.spizag().date.getDay()]+",",r.spizag().date.getDate()+"/"+(r.spizag().date.getMonth()+1)+"/"+r.spizag().date.getFullYear());
  console.log("Julian tithi time, mean:", (r.tithi().mean*1).toFixed(6)+", true:", (r.tithi().true*1).toFixed(6));
  console.log("\nNew date, Tshespa/lunar day, Month, Year, Report, Previous lunar day, Exit\n\n")

}


console.log("[OK] Testing",tr.label);
console.log("\n\n"+process.env.KALACAKRA_TRADITION_NUMBER+".",tr.label)

for (y=(process.env.KALACAKRA_TEST_FROM_YEAR||2019); y<=(process.env.KALACAKRA_TEST_TO_YEAR||2019); y++) {
  for (m=1; m<=12; m++) {
    console.log("[OK] Month",y,m);
    for (d=1; d<=30; d++) {
      r = tr.calculate(y, m, d);
      day_details(r);
    }
    // If the month correction has been active, then we need to rerun this month with the flag to catch up.
    if (r.zla().zeromthfg) {
      console.log("[OK] Repeating month",y,m);
      mr = []
      for (d=1; d<=30; d++) {
         r = tr.calculate(y, m, d, true);
         day_details(r);
      }
    }
  }
}
