#!/bin/bash

# Daybreak -- EH program give unlikely values for daybreak (negative inner radix)
# sgos zhag -- planets, not yet supported

# This is the Generalised Tsurphu tradition from Henning's original software.

export KALACAKRA_TRADITION=../kalacakra/tsurphu-general.js
export KALACAKRA_TRADITION_NUMBER=2 # number of the menu option, not required :)

node test_day_details.js    | grep -v "Daybreak" > a
cat compare/day_details_ts_general_2019.txt | grep -v "Daybreak" | grep -v "sgos zhag" > b
#diff --side-by-side a b
diff --suppress-common-lines --side-by-side a b

# Now the two Tsurphu traditions from the revised software, which has slightly different output.

export KALACAKRA_TRADITION=../kalacakra/tsurphu-kongtrul.js
export KALACAKRA_TRADITION_GRUB=../kalacakra/tsurphu-kongtrul-grub.js
export KALACAKRA_TRADITION_NUMBER=1

node test_tsurphu_day_details.js    | grep -v "Daybreak" > a
cat compare/tsurphu_day_details_kongtrul_2019.txt | grep -v "Daybreak" | grep -v "sgos zhag" > b
# diff --side-by-side a b
diff --suppress-common-lines --side-by-side a b

export KALACAKRA_TRADITION=../kalacakra/tsurphu-tantra.js
export KALACAKRA_TRADITION_GRUB=../kalacakra/tsurphu-tantra-grub.js
export KALACAKRA_TRADITION_NUMBER=2

node test_tsurphu_day_details.js    | grep -v "Daybreak" > a
cat compare/tsurphu_day_details_tantra_2019.txt | grep -v "Daybreak" | grep -v "sgos zhag" > b
# diff --side-by-side a b
diff --suppress-common-lines --side-by-side a b
