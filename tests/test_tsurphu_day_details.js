const math = require('mathjs');
const tr = require(process.env.KALACAKRA_TRADITION||'../kalacakra/tsurphu-kongtrul.js');
const trg = require(process.env.KALACAKRA_TRADITION_GRUB||'../kalacakra/tsurphu-kongtrul-grub.js');
const util = require('../kalacakra/lib/util.js')

const weekdays = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
const months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Dec" ];

function day_details(r, rg) {
  var henning_month = r.zla().month;
  if (r.zla().intercalary) {
    henning_month = -henning_month;
  }

  console.log("\n\n"+process.env.KALACAKRA_TRADITION_NUMBER+".",tr.label)
  console.log("The ZLA-DAG is:", util.str_henning(r.zla().zladag, [65], false, false, 1)+", Month =", r.unadjusted_month()+", Year =", r.year());
  console.log("DATE is:", r.day(), "/", r.unadjusted_month(), "/", r.year()+",  Corrected Month =", henning_month+",", r.month_cycles().month('bo_wylie'));
  console.log('Rahu month:', r.rahu()[15].month+', gdong, 15th:', util.str_henning(r.rahu()[15].gdong, [60, 60, 6], false, false, 4), '- 30th:', util.str_henning(r.rahu()[30].gdong, [60, 60, 6], false, false, 4));
  console.log("ril cha =", util.str_henning(r.rilcha(), [126], false, false, 1));
  console.log("The gza-bar is:", util.str_henning(r.gza().tsebar, [60, 60, 6, 707], false, true, 4));
  console.log("The (grub) nyi-bar is:", util.str_henning(rg.nyi().nyibar.longitude, [60, 60, 6], false, true, 5), "-", (rg.nyi().nyibar.degrees).toFixed(6));
  console.log("The (byed) nyi-bar is:", util.str_henning(r.nyi().nyibar.longitude, [60, 60, 6], false, true, 5), "-", (r.nyi().nyibar.degrees).toFixed(6));
  console.log("The gza-dag is:", util.str_henning(r.gza().gzadag, [60, 60, 6, 707], false, true, 4));
  console.log("The (grub) nyi-dag is:", util.str_henning(rg.nyi().nyidag.longitude, [60, 60, 6], false, true, 5), "-", (rg.nyi().nyidag.degrees).toFixed(6));
  console.log("The (byed) nyi-dag is:", util.str_henning(r.nyi().nyidag.longitude, [60, 60, 6], false, true, 5), "-", (r.nyi().nyidag.degrees).toFixed(6));
  // Henning's code gives unlikely numbers for the daybreak value (negative fractional parts)
  console.log("Moon, lunar day:", util.str_henning(r.monsar().mondag, [60, 60, 6], false, false, 5), "- Daybreak:", util.str_henning(r.monsar().monsar, [60, 60, 6], false, false, 5)+".");
  console.log("General day factors:", r.spizag().frac_b*1+",", r.spizag().frac_c*1);
/* TODO: planets
  console.log("Mars sgos zhag =", r.mars.gozhag+":", util.str_henning(r.mars.daldag, [60, 60, 6, tr.mars.frac], false, false, 4));
  console.log("Jupiter sgos zhag =", r.jupiter.gozhag+":", util.str_henning(r.jupiter.daldag, [60, 60, 6, tr.jupiter.frac], false, false, 4));
  console.log("Saturn sgos zhag =", r.saturn.gozhag+":", util.str_henning(r.saturn.daldag, [60, 60, 6, tr.saturn.frac], false, false, 4));
  console.log("Mercury sgos zhag =", r.mercury.gozhag+":", util.str_henning(r.mercury.kanbar, [60, 60, 6, tr.mercury.frac], false, false, 4));
  console.log("Venus sgos zhag =", r.venus.gozhag+":", util.str_henning(r.venus.kanbar, [60, 60, 6, tr.venus.frac], false, false, 4));
  */
  var date = r.spizag().date;
  console.log("SPYI ZHAG =", r.spizag().spizag+". Julian day =", r.spizag().juldat+". Western date =",weekdays[date.getDay()]+",",date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear());
  console.log("Julian tithi time, mean:", (r.tithi().mean*1).toFixed(6)+", true:", (r.tithi().true*1).toFixed(6));
  console.log("\nNew date, Tshespa/lunar day, Month, Year, Report, Previous lunar day, Exit\n\n\n")

}


console.log("[OK] Testing",tr.label);
console.log("\n\n\n"+process.env.KALACAKRA_TRADITION_NUMBER+".",tr.label);
console.log("Enter the starting date, month and year: \n");

for (y=(process.env.KALACAKRA_TEST_FROM_YEAR||2019); y<=(process.env.KALACAKRA_TEST_TO_YEAR||2019); y++) {
  for (m=1; m<=12; m++) {
    console.log("[OK] Month",y,m);
    for (d=1; d<=30; d++) {
      r = tr.calculate(y, m, d);
      rg = trg.calculate(y, m, d);
      day_details(r, rg);
    }
    // If the month correction has been active, then we need to rerun this month with the flag to catch up.
    if (r.zla().zeromthfg) {
      console.log("[OK] Repeating month",y,m);
      mr = []
      for (d=1; d<=30; d++) {
         r = tr.calculate(y, m, d, true);
         rg = trg.calculate(y, m, d, true);
         day_details(r, rg);
      }
    }
  }
}
