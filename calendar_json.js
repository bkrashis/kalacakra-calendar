tr = require('./kalacakra/phugpa.js');

var y = new Date().getFullYear();

yr = []
for (m=1; m<=12; m++) {
    mr = []
    for (d=1; d<=30; d++) {
       result = tr.calculate(y, m, d, false);
       delete result.work;
       if (result.lhag && typeof result.lhag.work !== 'undefined') {
         delete result.lhag.work;
       }
       mr.push(result);
    }
    yr.push(mr);
    // If the month correction has been active, then we need to rerun this month with the flag to catch up.
    if (mr[mr.length-1].zeromthfg) {
      mr = []
      for (d=1; d<=30; d++) {
         result = tr.calculate(y, m, d, true);
         mr.push(result);
      }
      yr.push(mr);
    }
}

console.log(JSON.stringify(yr));
