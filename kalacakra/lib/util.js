bn = require('bn.js');
fr = require('fraction.js');

function lazy_value(f, name) {
  if (typeof f !== "function") {
    return function() { return f }
  } else {
    var x = null;
    return function(force) {
      if (!x || force) {
        x = f();
      }
      if (!x) {
        console.log('Suspicious return value',x,'for',name);
      }
      return x;
    }
  }
}

function nameable(nameset,idx) {
  if (typeof idx.floor !== 'undefined') {
    idx = idx.floor()*1;
  } else {
    idx = Math.floor(idx);
  }

  return function(lang) {
    if (!lang) return idx;
    if (typeof nameset[lang] === "undefined" || !nameset[lang]) return null;
    return nameset[lang][idx % nameset[lang].length];
  };
}

function fraction(n,d) {
  return new fr.Fraction({n: new bn.BN(n), d: new bn.BN(d)})
}

function chain(n) // Return a fraction represented as successive smaller fractions: [[2, 60], [3, 60], [5, 6]...]
{
    var d = new bn.BN(1);
    var r = 0;
    for (var i=0; i<n.length; i++) {
        d = d.mul(new bn.BN(n[i][1]));
        r = fraction(new bn.BN(n[i][0]), d).add(r);
    }
    return r;
}

function radix(n,x) // Return a fraction represented as values and radix: ([2, 3, 5...], [60, 60, 6...])
{
    var d = new bn.BN(1);
    var r = 0;
    for (var i=0; i<n.length; i++) {
        d = d.mul(new bn.BN(x[i]));
        r = fraction(new bn.BN(n[i]), d).add(r);
    }
    return r;
}

function round(f, base) {
  if (!base) {
    base = 1;
  }
  if (f.s == 1) {
    return f.mul(base).floor().div(base);
  } else {
    return f.mul(base).ceil().div(base);
  }
}

function get_chain(f, l)
{
    var r = [];
    for (var i=0; i<=l.length; i++) {
        r.push(f.abs().floor());
        frac = f.mod(1);
        f = frac.mul(l[i]);
    }
    r.push(frac);

    return r;
}

function str_henning(f, l, include_base, add_spaces, fixed_length, include_sign)
{
    var r = get_chain(f, l);

    var base = "";
    if (include_base) {
        base += " ("+l;
        if (r[r.length-1].d > 1) {
            base += ","+r[r.length-1].d;
        }
        base += ")";
    }

    var eff_length = fixed_length;
    if (!fixed_length) {
      for (var j=0; j<r.length; j++) {
          if (j>eff_length && r[j].n!=0) { eff_length = j; }
      }
    }

    var out = "";
    if (include_sign && f.s == -1) {
      out = "-";
    }
    out += r[0].n;

    for (var i=1; i<=eff_length; i++) {
        if (i==1) { out+=";"; }
        else { out+=","; }
        if (add_spaces) { out+=" " }
        if (i<r.length) { out += r[i].n }
        else { out += "0" }
    }

    return out+base;
}

module.exports = {
  lazy_value: lazy_value,
  nameable: nameable,

  fraction: fraction,
  radix: radix,
  chain: chain,
  get_chain: get_chain,
  str_henning: str_henning,
  round: round,

  circle: fraction(27, 1),
  half_circle: fraction(27, 2)
};
