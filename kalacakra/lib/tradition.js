util = require('./util.js');
cycles = require('./cycles.js');
planets = require('./planets.js');
zla = require('./zla.js');
common = require('./common.js');

/* Main entry point for calculations */
const LHAG_CHECK = 1;
const LHAG_FOUND = 2;

function calculate(year, unadjusted_month, day, zeromthfg, obs_long, lhag_status) {
  var tr = this;

  if (!obs_long) {
    obs_long = 91.0 // Observer's longitude, default to Lhasa at 91.0 E
  }

  var work = {
    day: util.lazy_value(day, 'day'),
    unadjusted_month: util.lazy_value(unadjusted_month, 'unadjusted_month'),
    year: util.lazy_value(year, 'year'),
    obs_long: util.lazy_value(obs_long, 'obs_long'),
    zeromthfg: util.lazy_value(zeromthfg, 'zeromthfg'),
    is_lhag: lhag_status==LHAG_FOUND // TRUE if this object represents the added day. "Lhag" days (i.e. days with "lhag") have false here and an additional "lhag" property.
  };

  // Iterate through all the properties in the tradition and set them up.
  // util.lazy_value won't evaluate them until they are used, so it doesn't matter what order we set them up in.
  for (var property in tr) {
    if (tr[property].func) {
      work[property] = (function(closure_property) {
          return util.lazy_value(function() {
              return tr[closure_property].func(work)
          }, closure_property);
      }(property));
    }
  }

  var prv_result = null;
  if (!lhag_status) {
    // First we need to run the calculations for yesterday, to find out if we have a LHAG day.
    // It's also useful to return both lunar days which are in effect over the course of one solar day.
    // TODO: This assumes we can get away with day = 0, zeromthfg, etc over month boundaries. Is day 1 of a month ever LHAG?
    prv_result = util.lazy_value(function() { return tr.calculate(year, unadjusted_month, day-1, zeromthfg, obs_long, LHAG_CHECK) });
    days_since_prv = work.spizag().juldat.floor() - prv_result().spizag().juldat.floor();
    switch (days_since_prv) {
      case 0: // chad
        work.lhag = util.lazy_value(null); // so that clients don't have to test for it differently
        work.chad = util.lazy_value(true);
        break;
      case 1: // normal day
        work.lhag = util.lazy_value(null); // so that clients don't have to test for it differently
        work.chad = util.lazy_value(false);
        break;
      case 2: // lhag
        work.lhag = util.lazy_value(function() { return tr.calculate(year, unadjusted_month, day, zeromthfg, obs_long, LHAG_FOUND) });
        work.chad = util.lazy_value(false);
        break;
      default:
        console.error("Day was "+days_since_prv+" julian days after previous day.");
    }
  }

  return work;
}

function get_tradition(partial_tradition) {
  return base_tradition.add(partial_tradition);
}

function add_tradition(partial_tradition) {
    var full_tradition = {};
    Object.assign(full_tradition, this);
    for (var key in partial_tradition) {
        if (this[key] && this[key]._merge) {
          full_tradition[key] = add_tradition.call(full_tradition[key],partial_tradition[key]);
          //Object.assign(full_tradition[key], partial_tradition[key])
        } else {
          full_tradition[key] = partial_tradition[key]
        }
    }
    return full_tradition;
}

/* include default settings for many traditions */

module.exports = {
    label: "Base tradition, do not use without adding other traditions",
    calculate: calculate,
    add: add_tradition,
    year_cycles: {
      _merge: true,
      func: function(work) { return cycles.year(this, work.year()); }
    },
    month_cycles: {
      _merge: true,
      func: function(work) { return cycles.month(this, work.year_cycles(), work.zla().month); }
    },
    solar_day_cycles: {
      _merge: true,
      func: function(work) { return cycles.solar_day(this, work.spizag().juldat) },
      weekday_four_elements: [ 0, 2, 1, 2, 1, 3, 0 ]
    },
    lunar_day_cycles: {
      _merge: true,
      func: function(work) { return cycles.lunar_day(this, work.month_cycles(), work.day()); }
    },
    lunar_mansion_cycles: {
      _merge: true,
      func: function(work) { return cycles.lunar_mansion(this, work.monsar().monsar); },
      four_elements: [ 3, 2, 2, 0, 3, 1, 3, 2, 1, 2, 2, 3, 3, 3, 3, 2, 0, 0, 1, 1, 0, 0, 1, 0, 2, 1, 1], // EH: taken from Jampa Gyaltsen
    },
    zla: {
        _merge: true,
        zladag: {
            _merge: true,
            add: util.radix([0, 0], [1, 65]),
            mul: util.radix([1, 2], [1, 65])
        },
        adjust_func: zla.adjust([48, 49]),
        func: function(work) { return zla.zla(this, work.year(), work.unadjusted_month(), work.zeromthfg()); }
    },
    rilcha: {
        _merge: true,
        mul: util.radix([ 2, 1], [  1, 126]),
        func: function(work) { return common.rilcha(this, work.zla().true_month); }
    },
    nyi: {
        _merge: true,
        nyidru: { // nyidru
            _merge: true,
            mul: util.radix([2, 10, 58, 1, 17], [1, 60, 60, 6, 67]) // EH nyidmg
        },
        nyilon: { // nyilon
            _merge: true,
            mul: util.radix([0, 4, 21, 5, 43], [1, 60, 60, 6, 67]) // EH nyilmg
        },
        frac: 67,
        fac: util.radix([6, 45], [1, 60]), // nyifac
        bye: [6, 4, 1, -1, -4, -6], // nyibye
        dom: [0, 6, 10, 11, 10, 6], // nyidom
        func: function(work) { return planets.sun(this, work.zla().true_month, work.day()); }
    },
    gza: {
        _merge: true,
        gzadru: {
            _merge: true,
            mul: util.radix([1, 31, 50, 0, 480], [1, 60, 60, 6, 707]) // EH gzadmg
        },
        tsedru: {
            _merge: true,
            mul: util.radix([0, 59, 3, 4, 16], [1, 60, 60, 6, 707]) // EH tsedmg
        },
        bye: [5, 5, 5, 4, 3, 2, 1, -1, -2, -3, -4, -5, -5, -5],
        dom: [0, 5, 10, 15, 19, 22, 24, 25, 24, 22, 19, 15, 10, 5],
        frac: 707,
        func: function(work) {
          return common.gza(this, work.zla().true_month, work.day(), work.rilcha(), work.nyi().correction, work.nyi().frac, work.is_lhag);
        }
    },
    monsar: {
      _merge: true,
      frac: 707,
      func: function(work) {
        return common.monsar(this, work.day(), work.nyi().nyidag.longitude, work.gza().gzadag, work.is_lhag);
      }
    },
    sbyorba: {
      _merge: true,
      func: function(work) {
        // EH: This is strictly wrong, we should use the Sun's longitude at daybreak,
        // EH: but in the Tibetan tradition such an adjustment is not made
        return common.sbyorba(this, work.monsar().monsar, work.nyi().nyidag.longitude);
      }
    },
    byedpa: {
      _merge: true,
      func: function(work) {
        // EH: At daybreak for moon
        return common.byedpa(this, work.monsar().monsar, work.nyi().nyidag.longitude);
      }
    },
    phrochen: {
      _merge: true,
      func: function(work) {
        return common.phrochen(this, work.solar_day_cycles().weekday, work.monsar().monsar);
      }
    },
    spizag: {
      _merge: true,
      javascript_epoch: 2440587.5,
      javascript_ticks_per_day: 86400000, // Ignoring leap seconds *shrug*
      func: function(work) {
        return common.spizag(this, work.zla().true_month, work.day(), work.gza().gzadag, work.is_lhag);
      }
    },
    dragkang: {
      // DRAG GSUM RKANG 'DZIN, ZHI GNYIS DAL BAR, KTC 63
      _merge: true,
      mul: 18382,
      mod: 6714405, // == 149209 * 45
      frac: 149209,
      func: function(work) { return common.dragkang(this, work.spizag().spizag); }
    },
    tithi: {
      _merge: true,
      func: function(work) { return common.tithi(this, work.spizag().juldat, work.gza().tsebar, work.gza().gzadag, work.obs_long()); }
    },
    nyindhru: {
      _merge: true,
      frac: 149209,
      func: function(work) { return work.dragkang().dragkang; }
    },

    rahu: {
      _merge: true,
      tsa: util.radix([0, 0, 14, 0, 12], [1, 60, 60, 6, 23]),
      func: function(work) {
        return { 15: planets.rahu(this, work.zla(0).true_month, 15),
                 30: planets.rahu(this, work.zla(0).true_month, 30) };
      }
    },

    planets: planets
};

/* http://www.kalacakra.org/calendar/byed_ex.htm
const example = {

    label: "Simplified tradition for worked examples",
    nyidru: {
        add: util.radix([26, 58], [1, 60]),
        mul: util.radix([2, 10, 58, 2, 10], [1, 60, 60, 6, 13])
    },
    gzadru: {
        add: util.radix([2, 30], [1, 60]),
        mul: util.radix([1, 31, 50], [1, 60, 60])
    },
    tsedru: {
        mul: util.radix([0, 59, 3, 4], [1, 60, 60, 6])
    },
    nyilon: {
        mul: util.radix([0, 4, 21, 5, 9], [1, 60, 60, 6, 13])
    }

  };
*/
