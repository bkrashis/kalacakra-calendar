// Calculates the true month

function zla(zla_tradition, year, unadjusted_month, zeromthfg) {
  // v.27: The count of years and the true number of months (zla ba'i tshogs rnam par dag pa).
  var months_since_tradition = 12*(year - zla_tradition.year) + unadjusted_month - 3;
  if ( zla_tradition.khrulsel ) a += 1;

  // EH sets zladag to 0 if months is negative but I suspect that's because he is using unsigned arithmetic.
  zladag = zla_tradition.zladag.mul.mul(months_since_tradition)
                    .add(zla_tradition.zladag.add);

  var adjzla = zla_tradition.adjust_func(zladag, zeromthfg);

  var month = (unadjusted_month+adjzla.month_adjust+11) % 12 + 1;

  return {
    month: month,
    zladag: adjzla.zladag,
    true_month: adjzla.zladag.floor(),
    intercalary: adjzla.intercalary,
    zeromthfg: adjzla.zeromthfg
  }
}

function adjust(zlasho) {
  return function(zladag, zeromthfg) {
    var zlarem = zladag.mod(1).mul(65).floor();

    if (zlarem <= 1) {
      if (!zeromthfg) {
        return { zladag: zladag.sub(1), zeromthfg: true, month_adjust: -1, intercalary: false };
      } else {
        // Henning's code adds 1 to zladag here, but somehow mine doesn't need to???
        return { zladag: zladag, zeromthfg: false, month_adjust: 0, intercalary: false };
      }
    } else if (zlarem >= zlasho[0] && zlarem <= zlasho[1]) {
      return { zladag: zladag, zeromthfg: zeromthfg, month_adjust: 0, intercalary: true };
    } else if ( zlarem > zlasho[1] ) {
      return { zladag: zladag, zeromthfg: zeromthfg, month_adjust: -1, intercalary: false };
    }

    return { zladag: zladag, zeromthfg: zeromthfg, month_adjust: 0, intercalary: false };
  }
}

module.exports = {
  zla: zla,
  adjust: adjust
}
