util = require('./util.js');

function year(tr, yr) {
  var y = Math.floor(yr);

  var a = (y-7) % 12;
  var e = (Math.floor(y/2)-3) % 5;
  var g = y % 2;
  return {
    animal: util.nameable(tr.names.animals, a),
    element: util.nameable(tr.names.elements, e),
    gender: util.nameable(tr.names.genders, g),
  };
}

function month(tr, year_cycles, mo) {
  var m = Math.floor(mo);

  var a = m % 12;
  var cm = (m + 2) % 12;
  var e;
  if (year_cycles.gender == 0 && m>=11) {
    e = year_cycles.element();
  } else {
    e = year_cycles.element() + 1;
  }
  e = Math.floor(e + (m+1)/2) % 5;
  var g = (m+1) % 2;
  return {
    month: util.nameable(tr.names.months, mo),
    animal: util.nameable(tr.names.animals, a),
    element: util.nameable(tr.names.elements, e),
    gender: util.nameable(tr.names.genders, g),
    chinese_month: cm  // FIXME nameable?
  };
}

function lunar_day(tr, month_cycles, day) {
  var cm = month_cycles.chinese_month;
  var d = Math.floor(day);

  var day_of_year = (cm+11) * 30 + d;
  var par = day_of_year % 8;
  // EH: If Chinese month is number 1, "sme ba" is 1
  var sme = day_of_year % 9;
  // EH: If Chinese month is number 1, Animal is Tiger, index = 11
  var a = (day_of_year + 10) % 12;
  return {
    trigram: util.nameable(tr.names.trigrams, par),
    animal: util.nameable(tr.names.animals, a),
    smeba: sme || 9, // 0 -> 9
    colour: util.nameable(tr.names.smeba_colours, sme)
  }
}

function solar_day(tr, juldat) {
  var j = Math.floor(juldat);

  var a = ( j - 2 ) % 12;
  var e = ( ( j - 3 ) / 2 ) % 5;
  var clm = ( j - 17 ) % 28;
  var sme = ( j - 2 ) % 9 + 1;
  var weekday = ( j - 5 ) % 7;
  var welem = tr.weekday_four_elements[weekday];
  return {
    animal: util.nameable(tr.names.animals, a),
    element: util.nameable(tr.names.elements, e),
    weekday: util.nameable(tr.names.weekdays, weekday),
    weekday_four_element: util.nameable(tr.names.four_elements, welem),
    chinese_lunar_mansion: util.nameable(tr.names.chinese_lunar_mansions, clm),
    smeba: sme
  }
}


function lunar_mansion(tr, monsar) {
  var lunman = Math.floor(monsar);
  var e = tr.four_elements[lunman];
  return {
    lunar_mansion: util.nameable(tr.names.lunar_mansions, lunman),
    element: util.nameable(tr.names.four_elements, e)
  }
}

module.exports = {
  year: year,
  month: month,
  lunar_day: lunar_day,
  solar_day: solar_day,
  lunar_mansion: lunar_mansion
}
