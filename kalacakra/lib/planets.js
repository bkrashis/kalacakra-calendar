// Calculations for planets and for the Sun, which uses the same technique
util = require('./util.js'); // FIXME

function zodiac(angle) {
  var zodiac = angle.div(27).mul(12); // convert scale from 0..27 into 0..12 (12 zodiac signs)
  var degrees = angle.div(27).mul(360)*1;
  return {
    sign: zodiac,
    longitude: angle,
    degrees: degrees
  }
}

function dalbar_to_daldag(planet_tradition, dalbar) {
  work = {};
  work.wor = dalbar.sub(planet_tradition.fac).add(27).mod(27);

  if (work.wor < util.half_circle) { // top half of circle
    work.dor = false;
  } else {
    work.dor = true;
    work.wor = work.wor.sub(util.half_circle);  // subtract half circle
  }

  work.big = util.round(work.wor, 60);
  work.tquo = work.big.mul(60).div(135).floor();
  work.trem = work.big.mul(60).mod(135);
  work.small = work.wor.sub(work.big);
  work.lista = work.trem.div(60)
                  .add(work.small)
                  .mul(planet_tradition.bye[work.tquo])
                  .div(135);
  work.lista = util.round(work.lista,60*60*6*planet_tradition.frac);
  work.listb = util.fraction(planet_tradition.dom[work.tquo], 60);

  var correction = work.listb.add(work.lista);

  if (!work.dor) {
    correction = correction.neg();
  }
  var daldag = dalbar.add(correction).mod(27);

  return {
    daldag: daldag,
    correction: correction,
    _debug: work
  }
}

// Calculate solar quantities
function sun(sun_tradition, true_month, day) {
  // v.29/30. Monthly mean solar longitude.

  var nyidru = sun_tradition.nyidru.mul.mul(true_month)
                  .add(sun_tradition.nyidru.add)
                  .mod(27);

  var nyilon = sun_tradition.nyilon.mul.mul(day);
  //  v.33-35. Correction for the true solar longitude.

  var nyibar = nyilon.add(nyidru)
                  .mod(27);

  var ddr = dalbar_to_daldag(sun_tradition, nyibar);

  return {
    nyilon: zodiac(nyilon),
    nyibar: zodiac(nyibar),
    nyidru: nyidru,
    nyidag: zodiac(ddr.daldag),
    correction: ddr.correction,

    frac: sun_tradition.frac, // need to know this so gza can round off properly
    _debug: ddr._debug
  }
}

function rahu(rahu_tradition, true_month, day) {
  var month = true_month.add(rahu_tradition.part).mod(230);
  var t = month.mul(30).add(day);
  var gdong = util.circle.sub(t.mul(rahu_tradition.tsa).mod(27)) // rahudong
  // rahujug = rahudong + half-circle, but this is never used.

  return {
    month: month,
    gdong: gdong
  }
}

function planet_inner(work) { // this == tradition.planet
  var spizag = work.spizag.spizag;
  var nyindhru = work.nyindhru;

  var gozhag = spizag.mul(100)
              .add(this.add)
              .mod(this.cycle);
  var kanbar = gozhag.mul(27).div(this.cycle); // convert days in 0..this.cycle into a standard angle in 0..27
  var daldag = dalbar_to_daldag(this, nyindhru);

  return {
    gozhag: gozhag,
    kanbar: kanbar,
  };
}

function planet_outer(work) { // this == tradition.planet
  var spizag = work.spizag.spizag;

  var gozhag = spizag.mul(100)
              .add(this.add)
              .mod(this.cycle);
  var dalbar = gozhag.mul(27).div(this.cycle); // convert days in 0..this.cycle into a standard angle in 0..27
  var daldag = dalbar_to_daldag(this, dalbar);

  return {
    gozhag: gozhag,
    dalbar: dalbar,
    daldag: daldag.daldag
  };
}

module.exports = {
  sun: sun,
  rahu: rahu,
  planet_inner: planet_inner,
  planet_outer: planet_outer
}
