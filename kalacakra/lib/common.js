util = require('./util.js');

function rilcha(rilcha_tradition, true_month) {
  // v.29. The lunar anomaly (ril po dang cha shas).
  return rilcha_tradition.mul.mul(true_month)
                .add(rilcha_tradition.add)
                .mod(28);
}

function sbyorba(sbyorba_tradition, moon_long, sun_long) {
    // EH: Now calculate yoga, sbyor ba:
    var sbyorba = moon_long.add(sun_long)
                    .mod(27);
    return {
      name: util.nameable( sbyorba_tradition.names, sbyorba ),
      value: sbyorba
    }
}

function byedpa(byedpa_tradition, moon_long, sun_long) {
    // EH: Now calculate karana, byed pa:
    var byedpa = moon_long.sub(sun_long)
                    .add(27).mod(27)     // wrap around negative values
                    .div(27).mul(60);    // convert scale from 0..27 to 0..60

    var byedpa_index = byedpa.floor()*1;
    switch (byedpa_index) {
      case 0: byedpa_index = 7; break;
      case 57: byedpa_index = 8; break;
      case 58: byedpa_index = 9; break;
      case 59: byedpa_index = 10; break;
      default: byedpa_index = (byedpa_index-1) % 7;
    }

    return {
      name: util.nameable( byedpa_tradition.names, byedpa_index ),
      value: byedpa
    }
}

function tithi(tithi_tradition, juldat, tsebar, gzadag, obs_long) {
  //  EH: To calculate exact Julian day for TRUE lunar day (tithi):
  var jdf_t = juldat.sub( util.fraction(obs_long, 360) )  // Observer's longitude
          .sub( util.fraction( 7, 24 ) )                  // EH: Julian day at 5.00 LMST
          .add( gzadag.mod(1) );                          // EH: Now add time of true tithi

  // EH: NOW CALCULATE FOR MEAN TITHI
  // + 10 % 7 - 3 maps [-6, -1, 1, 6] to [1, -1, 1, -1]. There should be no other values.
  var day_offset = ( util.round(tsebar)*1 - util.round(gzadag)*1 + 10 ) % 7 - 3
  var jdf_m = juldat.add( day_offset )
          .sub( util.fraction(obs_long, 360) )
          .sub( util.fraction( 7, 24 ) )                  // EH: Julian day at 5.00 LMST
          .add( tsebar.mod(1) );                          // EH: Now add time of mean tithi

  return {
    true: jdf_t,
    mean: jdf_m
  }
}

function monsar(monsar_tradition, day, nyidag, gzadag, is_lhag) {
  var mondag = util.fraction(54, 60)  // EH: 1/30th of a revolution
                  .mul(day)
                  .add(nyidag);

  if (is_lhag) {
    mondag = mondag.add(26) // i.e. subtract 1
  }

  mondag = mondag.mod(27);

  var monsar = mondag.sub(gzadag.mod(1)) // Also called 'monlong' elsewhere in EH code
                                .add(27).mod(27); // wrap around negative values

  // Because we subtracted gzadag in 707ths (or some other fraction), we have to round off again
  monsar = util.round(monsar,60*60*6*monsar_tradition.frac);

  return {
    mondag: mondag,
    monsar: monsar
  }
}

function gza(gza_tradition, true_month, day, rilcha, correction, correction_frac, is_lhag) {
  // v.28. Monthly mean weekday (gza'i dhrū ba).
  // This calculates the mean weekday for the new moon at the beginning of the month.
  var gzadru = gza_tradition.gzadru.mul.mul(true_month)
                  .add(gza_tradition.gzadru.add)
                  .mod(7);


  // v.31-33. Calculation of the true weekday.
  var tsedru = gza_tradition.tsedru.mul.mul(day)
  var tsebar = tsedru.add(gzadru)
                  .mod(7);

  var rilpo = rilcha.add(day)
                  .floor()
  var trem = rilpo.mod(14);

  var radj = rilcha.mod(1)
                  .mul(30*126)
                  .add(day)
                  .mul(gza_tradition.bye[trem]);

  // FIXME these bases aren't always the same
  var list1 = util.round(radj.div(30*126*60), 60*60*6*gza_tradition.frac);
  var list2 = util.fraction(gza_tradition.dom[trem], 60);
  var list3 = list1.add(list2);

  var gzawor;
  if (rilpo.mod(28) >= 14) {
      gzawor = tsebar.sub(list3);
  } else {
      gzawor = tsebar.add(list3);
  }

  // We next round down into the base belonging to the correction, so we can add it.
  // gzawor is a modulo quantity which may now be slightly negative, so add a week before rounding
  gzawor = util.round(gzawor.add(7), 60*60*6*correction_frac).mod(7);

  var gzadag = gzawor.add(correction)
                  .add(7).mod(7); // wrap around negative values

  if (is_lhag) {
    // The previous gzadag was just before a whole number, and this one is just after a whole number.
    // For the lhag day we use a gzadag of today's whole number.
    gzadag = gzadag.floor();
  } else {
    // Now we have to round down back into 707ths
    gzadag = util.round(gzadag,60*60*6*gza_tradition.frac);
  }

  return {
    tsebar: tsebar,
    gzawor: gzawor,
    gzadag: gzadag,
    gzadru: gzadru,
    _debug: {
      tsedru: tsedru,
      rilpo: rilpo,
      trem: trem,
      radj: radj,
      list1: list1,
      list2: list2,
      list3: list3
    }
  }
}

function spizag(spizag_tradition, true_month, day, gzadag, is_lhag) {
  // EH: calculate the general day
  var spizag = true_month.mul(30)
                  .add(day);

  var c = spizag.add(spizag_tradition.c);
  var spzfracc = c.mod(707); // FIXME

  var b = spizag.add(spizag_tradition.b)
                  .add( c.div(707).floor() );
  var spzfracb = b.mod(64);

  spizag = spizag.sub( b.div(64).floor() );
  var cc = spizag.add(spizag_tradition.f)
                 .mod(7);

  var bb = gzadag.floor();
  if ( cc != bb ) {
    if ( cc>4 && bb<2 ) { bb = bb.add(7); }
    else if ( bb>4 && cc<2 ) { cc = cc.add(7); }
    spizag = spizag.add(bb)
                   .sub(cc);
  }

  var juldat = spizag.add(spizag_tradition.j);
  if (is_lhag) {
    juldat = juldat.sub(1);
  }

  var date = new Date();
  date.setTime( Math.round(juldat.sub(spizag_tradition.javascript_epoch) * spizag_tradition.javascript_ticks_per_day) );
  return {
    spizag: spizag,
    frac_b: spzfracb,
    frac_c: spzfracc,
    juldat: juldat,
    date: date,
  }
}

function dragkang(dragkang_tradition, spizag) {
  // DRAG GSUM RKANG 'DZIN, ZHI GNYIS DAL BAR, KTC 63
  var dragkres = spizag.mul(dragkang_tradition.mul)
                .add(dragkang_tradition.add)
                .mod(dragkang_tradition.mod);
  var dragkang = dragkres.mul(27).div(dragkang_tradition.mod) // convert 0..this.mod into a standard angle in 0..27
  //              .mod(this.frac); // FIXME check,  this won't ever happen as 0<input<27

  return {
    dragkres: dragkres,
    dragkang: dragkang
  }
}

// VKP, 1,148
function phrochen(phrochen_tradition, weekday, monsar) {
  var x = monsar.floor()*1;
  if (monsar>21.5) {
    x++; // split number 21 into two halves
  }
  var px = 4 * (weekday() - 1);
  return {
    name: util.nameable(phrochen_tradition.names, (x - px + 28) % 28 )
  };
}

module.exports = {
  rilcha: rilcha,
  sbyorba: sbyorba,
  byedpa: byedpa,
  tithi: tithi,
  monsar: monsar,
  gza: gza,
  spizag: spizag,
  dragkang: dragkang,
  phrochen: phrochen
}
