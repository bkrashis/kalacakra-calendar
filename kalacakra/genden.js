zla = require('./lib/zla.js');
util = require('./lib/util.js');

/* constants and overrides for "dge ldan rtsis gsar" tradition */

genden = {
    label: "New Genden Calculations, 1747 CE.",
    zla: {
      year: 1747,
      zladag: {
          add: util.fraction(10,65), // 10 = eyr_a, Henning's "intercalation index"
          adjust_func: zla.adjust([46, 47]) // zlasho1, zlasho2
      },
    },
    nyi: {
      nyidru: {
        add: util.radix([26, 39, 51, 0, 18], [1, 60, 60, 6, 67]) // nda?[]
      }
    },
    gza: {
      gzadru: {
        add: util.radix([1, 55, 13, 3, 333], [1, 60, 60, 6, 707]) // gda?[]
      }
    },
    rilcha: {
        add: util.radix([24, 22], [1, 126]), // ril_a, ril_b
    },
    spizag: {
      b: 5, // spz_b
      c: 65, // spz_c
      f: 1, // spz_f
      j: 2359237 // spz_j
    },
    rahu: { part: 32 }, // rahupart, how far through the 230-month Rahu cycle
    planets: {
      mercury: { add: 2518 }, // meradd
      venus: { add: 1329 }, // venadd
      mars: { add: 375 }, // maradd
      jupiter: { add: 3213 }, // jupadd
      saturn: { add: 5147 }, // satadd
    },
    dragkang: {  // Calculated from longitude in text.
      add: 6632355 // dragkadd
    }
  };


module.exports = require('./lib/tradition.js')
                .add(require('./traditions/names.js'))
                .add(require('./traditions/events.js'))
                .add(require('./traditions/sadag.js'))
                .add(require('./traditions/names_byedpa_henning.js'))
                .add(genden);
