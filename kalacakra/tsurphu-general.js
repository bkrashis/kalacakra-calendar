util = require('./lib/util.js');

epoch = {
    label: "Generalised Tsurphu, -1000.",
    zla: {
      year: -1000,
      zladag: {
          add: util.fraction(11, 65) // 11 = eyr_a, EH "intercalation index"
      },
    },
    rilcha: {
        add: util.radix([6, 75], [1, 126]) // ril_a, ril_b
    },
    nyi: {
      nyidru: {
          add: util.radix([0, 7, 25, 0, 30], [1, 60, 60, 6, 67]) // nda2[]
      }
    },
    gza: {
        gzadru: {
            add: util.radix([5, 38, 22, 3, 701], [1, 60, 60, 6, 707]) // gda2[]
        }
    },
    spizag: {
      b: 50, // spz_b
      c: 66, // spz_c
      f: 5, // spz_f
      j: 1355847 // spz_j
    },
    rahu: { part: 93 }, // rahupart, EH: 230 fraction for Rahu cycle
    planets: {
      mercury: { add: 1977 }, // meradd
      venus: { add: 268 }, // venadd
      mars: { add: 3 }, // maradd
      jupiter: { add: 512 }, // jupadd
      saturn: { add: 2988 }, // satadd
    },
    dragkang: {  // EH: This is added for "drag po'i rkang 'dzin".
      add: 83343 // dragkadd
    }
  };

module.exports = require('./lib/tradition.js')
                .add(require('./traditions/names.js'))
                .add(require('./traditions/events.js'))
                .add(require('./traditions/sadag.js'))
                .add(require('./traditions/tsurphu.js'))
                .add(epoch);
