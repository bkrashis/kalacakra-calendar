util = require('./lib/util.js');

/* constants and overrides for "mkhas pa'i snying nor" tradition */

khenying = {
    label: "mkhas pa'i snying nor, 1796.",
    zla: {
      year: 1796,
      zladag: {
          add: util.fraction(16, 65), // 10 = eyr_a, EH: "intercalation index"
          // adjust_month as default (zlasho = 48, 49), EH: assumed due to solar terms used.
      }
    },
    nyi: {
      nyidru: {
        add: util.radix([26, 27, 45, 4, 2], [1, 60, 60, 6, 67]) // nda?[]
      }
    },
    gza: {
      gzadru: {
          add: util.radix([5, 24, 44, 1, 565], [1, 60, 60, 6, 707]) // gda?[]
      }
    },
    rilcha: {
        add: util.radix([8, 52], [1, 126]) // ril_a, ril_b
    },
    spizag: {
      b: 37, // spz_b
      c: 433, // spz_c
      f: 5, // spz_f
      j: 2377133 // spz_j
    },
    rahu: { part: 178 }, // rahupart, how far through the 230-month Rahu cycle
    planets: {
      mercury: { add: 6303 }, // meradd
      venus: { add: 522 }, // venadd
      mars: { add: 408 }, // maradd
      jupiter: { add: 3780 }, // jupadd
      saturn: { add: 1518 }, // satadd
    },
    dragkang: {  // Calculated from longitude in text.
      add: 6591588 // dragkadd
    }
  };


module.exports = require('./lib/tradition.js')
                .add(require('./traditions/names.js'))
                .add(require('./traditions/events.js'))
                .add(require('./traditions/sadag.js'))
                .add(require('./traditions/names_byedpa_henning.js'))
                .add(khenying);
