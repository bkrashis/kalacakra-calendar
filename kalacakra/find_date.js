function find_date(tr, target_date) {

    y = target_date.getFullYear();
    m = 1;
    d = 1;
    z = false;

    is_lhag = false;
    while (true) {
      result = tr.calculate(y, m, d, z);
      distance = (target_date - result.spizag().date) / 86400000;

      if (distance<1 && distance>=0) {
        break;
      }

      if (result.lhag()) {
        lhag_distance = (target_date - result.lhag().spizag().date) / 86400000;
        if (lhag_distance<1 && lhag_distance>=0) {
            is_lhag = true;
            break;
        }
      }

      daynum = d - 1 + m*30 + y*12*30 + distance;

      nd = Math.floor(daynum) % 30 + 1;
      nm = Math.floor(daynum/30) % 12;
      ny = Math.floor(daynum/30/12);

      if (result.zla().zeromthfg) { // need to look in the additional month
        if (nm==m+1 || (nm==0 && ny==y+1)) {
          nm = m; ny = y;
          z=!z;
        }
      }

      d = nd; m = nm; y = ny;
    }

    if (is_lhag) {
      return result.lhag();
    } else {
      return result;
    }
}

module.exports = {
    find_date: find_date
}
