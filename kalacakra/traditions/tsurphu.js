// Functions for the Tsurphu traditions.

function tsurphu_month(tr, year_cycles, mo, yr) {
  var a = (mo + 10) % 12;
  var cm = mo % 12;
  var e;
  if (year_cycles.gender == 0 && mo>=11) {
    e = Math.floor( year_cycles.element() + (mo+1)/2 );
  } else {
    var eff = tr.element_offset[year_cycles.gender()][year_cycles.element()];
    var eoff = (yr+1) % 5;
    if (eff!=eoff) { console.log(eff,eoff,":-("); }
    e = Math.floor( year_cycles.element() + (mo-3)/2 ) + eoff; // FIXME should be mo-1
  }
  e = e % 5;
  var g = (mo+1) % 2;
  return {
    month: util.nameable(tr.names.months, mo),
    animal: util.nameable(tr.names.animals, a),
    element: util.nameable(tr.names.elements, e),
    gender: util.nameable(tr.names.genders, g),
    chinese_month: cm
  };
}

function tsurphu_adjust() {
  return function(zladag, zeromthfg) {
    var zlarem = zladag.mod(1).mul(65).floor();

    if (zlarem <= 1) {
      if (!zeromthfg) {
        return { zladag: zladag.sub(1), zeromthfg: true, month_adjust: 0, intercalary: true };
      } else {
        // Henning's code adds 1 to zladag here, but somehow mine doesn't need to???
        return { zladag: zladag, zeromthfg: false, month_adjust: 0, intercalary: false };
      }
    }

    return { zladag: zladag, zeromthfg: zeromthfg, month_adjust: 0, intercalary: false };
  }
}

module.exports = {
    zla: {
      adjust_func: tsurphu_adjust() // see above (EH zlapure = 1)
    },
    month_cycles: {
      // The Tsurphu tradition adds different offsets to the element cycle
      // depending on what year it is.
      // This logic is copied directly from Henning but it actually looks
      // like it could be done in code (reading downwards it increases 1 each time)
      element_offset: [ // "Fire", "Earth", "Iron", "Water", "Wood"
                           [2,      4,       1,      3,       0], // male years
                           [3,      0,       2,      4,       1]  // female years
                      ],
      func: function(work) { return tsurphu_month(this, work.year_cycles(), work.zla().month, work.year()); }
    },
    events: {
      birth: { dates: ["4-8"] },
    },
    nyindhru: {
      // EH: For Tsurphu, use normal mean Sun
      frac: 13,
      func: function(work) { return work.nyi().nyibar.longitude }
    }
}
