// Names from the Kagyu Samye Ling monastery calendar.

module.exports = {
  sbyorba: {
    names: {
      bo_wylie: [ "sel", "mdza' bo", "tshe ldan", "skal bzang",
                      "bzang po", "rab stong", "las bzang", "'dzin pa",
                      "zer", "'bras", "'phel", "nge", "rman",
                      "dga' ba", "rdo rdje", "dngos grub", "lhung",
                      "dpa' bo", "yongs 'joms", "zhi ba", "grub",
                      "grub bya", "dge ba", "dkar po", "tshangs pa",
                      "dbang po", "sha 'khon"],
      en: [ "Elimination", "Loved one", "Living", "Good fortune",
                      "Good one", "Totally empty", "Good karma", "Possessor",
                      "Nail", "Grain", "Increase", "Certainty", "Wound",
                      "Joy", "Vajra", "Attainment", "Fall",
                      "Hero", "Terrace", "Peace", "Attainer",
                      "Attainment", "Virtue", "White", "Brahma",
                      "Indra", "Revenge"]
  }
}
