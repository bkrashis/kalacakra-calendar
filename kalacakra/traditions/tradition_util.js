// functions useful when defining traditions

function apply_func(obj, func, ...args) {
  out = {};
  for (var p in obj) {
    r = func.apply(obj[p], args);
    if (r) {
      out[p] = r;
    }
  }
  return out
}

function date_list(month, day) {
  if (this.dates) {
    return this.dates.includes(month+"-"+day);
  }
}

module.exports = {
  date_list: date_list,
  apply_func: apply_func
}
