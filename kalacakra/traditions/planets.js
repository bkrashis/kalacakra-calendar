planets = require('./lib/planets.js');

module.exports = {
  _merge: true,
  mercury: {
    _merge: true,
    func: planets.planet_inner,
    cycle: 8797,
    frac: 8797,
    fac: util.radix([16, 30], [1, 60]),
    bye: [7, 3, 3, -7, -10, -10],
    bye1: [16, 15, 14, 13, 11, 7, 5, 0, 4, 11, 20, 28, 34, 16],
    bye2: [28, 20, 11, 4, 0, 5, 7, 11, 13, 14, 15, 16, 16, 34],
    dom: [10,17,20,17,10,0],
    dom1: [16, 32, 47, 61, 74, 85, 92, 97, 97, 93, 82, 62, 34, 0],
    dom2: [34, 62, 82, 93, 97, 97, 92, 85, 74, 61, 47, 32, 16, 0]
  },
  venus: {
    _merge: true,
    func: planets.planet_inner,
    cycle: 2247,
    frac: 749,
    fac: util.radix([6, 0], [1, 60]),
    bye: [4,1,1,-4,-5,-5],
    dom: [5,9,10,9,5,0],
  },
  mars: {
    _merge: true,
    func: planets.planet_outer,
    cycle: 687,
    frac: 229,
    fac: util.radix([9, 30], [1, 60]),
    bye: [18,7,7,-18,-25,-25],
    dom: [25,43,50,43,25,0]
  },
  jupiter: {
    _merge: true,
    func: planets.planet_outer,
    cycle: 4332,
    frac: 361,
    fac: util.radix([12, 0], [1, 60]),
    bye: [9,3,3,-9,-11,-11],
    dom: [11,20,23,20,11,0]
  },
  saturn: {
    _merge: true,
    func: planets.planet_outer,
    cycle: 4332,
    frac: 361,
    fac: util.radix([18, 0], [1, 60]),
    bye: [15,6,6,-15,-22,-22],
    dom: [22,37,43,37,22,0]
  }
};
