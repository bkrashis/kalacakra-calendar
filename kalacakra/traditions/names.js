shared_names = {
  animals: {
    en: [ "Rabbit", "Dragon", "Snake", "Horse", "Sheep", "Monkey",
                      "Bird", "Dog", "Pig", "Mouse", "Ox", "Tiger" ],
    bo: [ "ཡོས་","འབྲུག་","སྦྲུལ་","རྟ་","ལུག་","སྤྲེའུ་","བྱ་","ཁྱི་","ཕག་","བྱི་བ་","གླང་","སྟག་" ],
    bo_wylie: [ "yos", "'brug", "sbrul", "rta", "lug", "spre'u",
                      "bya", "khyi", "phag", "byi ba", "glang", "stag" ]
  },
  elements: {
    bo: [ "མེ་","ས་","ལྕགས་","ཆུ་","ཤིང་" ],
    bo_wylie: [ "me", "sa", "lcags", "chu", "shing" ],
    en: [ "Fire", "Earth", "Iron", "Water", "Wood" ]
  },
  four_elements: {
    unicode: [ "🜃", "🜄", "🜂", "🜁" ],
    bo: [ "ས་","ཆི་","མེ་","རླུང་" ],
    bo_wylie: [ "sa", "chu", "me", "rlung" ],
    en: [ "Earth", "Water", "Fire", "Wind" ]
  },
  genders: {
    en: [ "male", "female" ],
    bo: [ "ཕོ་","མོ་" ],
    bo_wylie: [ "pho", "mo" ]
  }
};

module.exports = {
  year_cycles: {
    names: {
      animals: shared_names.animals,
      elements: shared_names.elements,
      genders: shared_names.genders
    }
  },
  month_cycles: {
    names: {
      months: {
          bo: [ "རྒྱལ་","མཆུ་","དབོ་","ནག་པ་","ས་ག་","སྣྲོན་","ཆུ་སྟོད་","གྲོ་བཞིན་","ཁྲུམས་","ཐ་སྐར་","སྨིན་དྲུག་","མགོ་" ],
          bo_wylie: [  "rgyal", "mchu", "dbo", "nag pa", "sa ga",	"snron", "chu stod", "gro bzhin",
                          "khrums", "tha skar", "smin drug", "mgo" ]
      },
      animals: shared_names.animals,
      elements: shared_names.elements,
      genders: shared_names.genders
    }
  },
  lunar_day_cycles: {
    names: {
      animals: shared_names.animals,
      trigrams: {
          unicode: [ "☴", "☲", "☷", "☱", "☰", "☵", "☶", "☳" ],
          bo: [ "ཟོན་","ལི་","ཁོན་","དྭ་","ཁེན་","ཁམ་","གིན་","ཟིན་" ],
          bo_wylie: [ "zon", "li", "khon", "dwa", "khen", "kham", "gin", "zin" ],
          zh_pinyin: [ "Xun", "Li", "Kun", "Dui", "Qian", "Kan", "Gen", "Zhen" ],
          en: [ "Wind", "Fire", "Earth", "Lake", "Sky", "Water", "Mountain", "Thunder" ],
          en_description: [ "Gentle", "Clinging", "Receptive", "Joyous", "Creative",
                               "Abysmal", "Keeping Still", "Arousing" ]
      },
      smeba_colours: {
          en: [ "Red", "White", "Black", "Blue", "Green", "Yellow", "White", "Red", "White" ] // from comment in tc.def
      },
    }
  },
  solar_day_cycles: {
    names: {
      animals: shared_names.animals,
      elements: shared_names.elements,
      four_elements: shared_names.four_elements,
      chinese_lunar_mansions: {
          zh_pinyin: [ "Jiao", "Kang", "Di", "Fang", "Xin", "Wei", "Ji", "Dou",
                              "Niu", "Nu", "Xu", "Wei", "Shi", "Bi", "Kui", "Lou",
                              "Wei", "Mao", "Bi", "Zui", "Can", "Jing", "Gui", "Liu",
                              "Xing", "Zhang", "Yi", "Zhen" ]
      },
      weekdays: {
        en: [ "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" ],
        bo: [ "སྤེན་མ་","ཉི་མ་","ཟླ་བ་","མིག་དམར་","ལྷག་པ་","ཕུར་བུ་","པ་སངས་" ],
        bo_wylie: [ "spen ma", "nyi ma", "zla ba", "mig dmar", "lhag pa", "phur bu", "pa sangs" ]
      }
    }
  },
  lunar_mansion_cycles: {
    names: {
      lunar_mansions: {
        bo: [ "ཐ་སྐར་","བྲ་ཉེ་","སྨིན་དྲུག་","སྣར་མ་","མགོ་","ལག་","ནབས་སོ་","རྒྱལ་",
        "སྐག་","མཆུ་","གྲེ་","དབོ་","མེ་བཞི་","ནག་པ་","ས་རི་","ས་ག་","ལྷ་མཚམས་","སྣྲོན་",
        "སྣྲུབས་","ཆུ་སྟོད་","ཆུ་སྨད་","གྲོ་ཞིན་","མོན་གྲེ་","མོན་གྲུ་","ཁྲུམས་སྟོད་","ཁྲུམས་སྨད་","ནམ་གྲུ་"],
        bo_wylie: [ "tha skar", "bra nye", "smin drug", "snar ma", "mgo",
                            "lag", "nabs so", "rgyal", "skag", "mchu", "gre", "dbo",
                            "me bzhi", "nag pa", "sa ri", "sa ga", "lha mtshams",
                            "snron", "snrubs", "chu stod", "chu smad", "gro zhin",
                            "mon gre", "mon gru", "khrums stod", "khrums smad",
                            "nam gru" ],
        en: [ "Horse-like", "Dancer", "Six Mothers", "Divine Ease", "Deer-Headed",
                            "Ferocious", "Goddess of Gifts", "Satisfier", "Dragon Tail", "Great Horse", "Small Horse", "Multi-coloured",
                            "Knowledge", "Little Black Bird", "Wind Goddess", "Power Goddess", "Hand",
                            "God of Power", "Root", "Higher Water", "Perfect, Accomplished", "God of Wind",
                            "Vanquisher", "Water Goddess", "Mountain Goddess", "Serpent Net",
                            "Increases Knowledge"],
        sa_iast: [ "Aśvinī", "Bharaṇī", "Kṛttikā", "Rohiṇī", "Mṛgaśiras",
                            "Ārdrā", "Punarvasu", "Puṣya", "Āśleṣā", "Maghā",
                            "Pūrvaphalgunī", "Uttaraphalgunī", "Hasta", "Citrā",
                            "Svāti", "Viśākhā", "Anurādhā", "Jyeṣṭhā", "Mūla",
                            "Pūrvāṣāḍhā", "Uttarāṣāḍhā", "Abhijit", "Dhaniṣṭhā",
                            "Śatabhiṣaj", "Pūrvabhādrapadā", "Uttarabhādrapadā",
                            "Revatī" ],
        sa_ascii: [ "Ashvini", "Bharani", "Krittika", "Rohini", "Mrigashiras",
                            "Ardra", "Punarvasu", "Pushya", "Ashlesha", "Magha",
                            "Purvaphalguni", "Uttaraphalguni", "Hasta", "Citra",
                            "Svati", "Vishakha", "Anuradha", "Jyeshtha", "Mula",
                            "Purvashadha", "Uttarashadha", "Abhijit", "Dhanishtha",
                            "Shatabhishaj", "Purvabhadrapada", "Uttarabhadrapada",
                            "Revati" ]
        },
        four_elements: shared_names.four_elements
    }
  },
  sbyorba: {
    names: {
      _merge: true,
      bo: [ "རྣམ་སེལ་","མཛའ་བོ་","ཚེ་དང་ལྡན་པ་","སྐལ་བཟང་","དགེ་བྱེད་","ཤིན་ཏུ་འགྲམས་",
      "ལས་བཟང་","འཛིན་བྱེད་","ཟུག་རྔུ","འགྲམས་","འཕེལ་","བརྟན་པ་","ཡོངས་བསྣུན","དགའ་བ་",
      "རྡོ་རྗེ་","དངོས་གྲུབ","ཕན་ཚུན","མཆོག་ཅན","ཡོངས་འཇོམས་","ཞི་བ་","གྲུབ་པ་","བསྒྲུབ་བྱ་",
      "དགེ་བ་","དཀར་པོ་","ཚངས་པ་","དབང་པོ་","འཁོན་འཛིན"],
      bo_wylie: [ "rnam sel", "mdza' bo", "tshe dang ldan pa", "skal bzang",
                      "dge byed", "shin tu 'grams", "las bzang", "'dzin byed",
                      "zug rngu", "'grams", "'phel", "brtan pa", "yongs bsnun",
                      "dga' ba", "rdo rje", "dngos grub", "phan tshun",
                      "mchog can", "yongs 'joms", "zhi ba", "grub pa",
                      "bsgrub bya", "dge ba", "dkar po", "tshangs pa",
                      "dbang po", "'khon 'dzin" ],
      sa_iast: [ "Viṣkambha", "Prīti", "Āyuśmān", "Saubhāgya", "Śobhana",
                 "Atigaṇḍa", "Sukarma", "Dhṛti", "Śūla", "Gaṇḍa", "Vṛddhi",
                 "Dhruva", "Vyāghatā", "Harṣaṇa", "Vajra", "Siddhi",
                 "Vyatipāta", "Variyas", "Parigha", "Śiva", "Siddha", "Sādhya",
                 "Śubha", "Śukla", "Brahma", "Māhendra", "Vaidhṛti" ],
      sa_ascii: [ "Vishkambha", "Priti", "Ayushmat", "Saubhagya", "Shobhana",
                        "Atiganda", "Sukarman", "Dhriti", "Shula", "Ganda",
                        "Vriddhi", "Dhruva", "Vyaghata", "Harshana", "Vajra",
                        "Siddhi", "Vyatipata", "Variyas", "Parigha", "Shiva",
                        "Siddha", "Sadhya", "Shubha", "Shukla", "Brahman",
                        "Indra", "Vaidhriti" ]
    }
  },
  byedpa: {
    names: {
      _merge: true,
      bo: [ "གདབ་པ་","བྱིས་པ་","རིགས་ཅན་","ཏིལ་རྡུང་","ཁྱིམ་སྐྱེས་","ཚོང་བ་","བི༹ཥཊི་",
      "མི་སྡུག་པ་","བཀྲ་ཤིས་","རྐང་བཞི་","ཀླུ" ],
      bo_wylie: [ "gdab pa", "byis pa", "rigs can", "til rdung",
                        "khyim skyes", "tshong ba", "viSTi", "mi sdug pa",
                        "bkra shis", "rkang bzhi", "klu" ],
      sa_iast: [ "Vava", "Valava", "Kaulava", "Taitila", "Gara", "Vaṇija",
                 "Viṣṭi", "Kintughna", "Śakuni", "Catuṣpada", "Nāga" ],
      sa_ascii: [ "Vava", "Valava", "Kaulava", "Taitila", "Gara", "Vanija",
                        "Vishti", "Kintughna", "Shakuni", "Catushpada", "Naga" ]
    }
  },
  phrochen: {
    names: {
      bo: [ "ཀུན་དགའ་","དུས་དབྱིག་","དུལ་བ་","སྐྱེ་དགུ་","གཞོན་","བྱ་རོག་","རྒྱ་མཚན་","དཔལ་བེའུ་",
      "རྡོ་རྗེ་","ཐོ་བ་","གདུགས་","གྲོགས་","ཡོད་","འདུད་བྱ་","མགལ་མེ་","རྩ་བཏོན་","ཆི་བདག་","མདའ་",
      "གྲུབ་པ་","མདུང་","བདུད་རྩི་","གཏན་ཤུང་","གླང་པོ་","སྟག་མྱོས་","ཟད་","གཡོ་","བརྟན་","འཕེལ་" ],
      bo_wylie: [ "kun dga'", "dus dbyig", "dul ba", "skye dgu", "gzhon",
                          "bya rog", "rgyal mtshan", "dpal be'u", "rdo rje", "tho ba",
                          "gdugs", "grogs", "yod", "'dud bya", "mgal me",
                          "rtsa bton", "chi bdag", "mda'", "grub pa", "mdung",
                          "bdud rtsi", "gtan shing", "glang po", "stag myos",
                          "zad", "g.yo", "brtan", "'phel"],
      en: [ "Bliss", "Staff of Time", "Discipline", "Multitude", "Youth",
                          "Crow", "Banner", "Knot", "Vajra", "Hammer",
                          "Parasol", "Friend", "Abundance", "Desires", "Firebrand",
                          "Uprooted", "Lord of Death", "Arrow", "Attainment", "Spear",
                          "Nectar", "Pestle", "Elephant", "Mad Tiger",
                          "Exhaustion", "Cunning", "Firmness", "Increase"]
    }
  }
}
