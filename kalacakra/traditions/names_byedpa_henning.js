// These names are produced by the phugpa version of Henning's code.
// The only difference is in the Wylie spelling of 'viSTi' as 'vishti'.
// Mainly useful for running the validation tests.

module.exports = {
  byedpa: {
    names: {
      bo_wylie: [ "gdab pa", "byis pa", "rigs can", "til rdung",
                        "khyim skyes", "tshong ba", "vishti", "mi sdug pa",
                        "bkra shis", "rkang bzhi", "klu" ]
    }
  }
}
