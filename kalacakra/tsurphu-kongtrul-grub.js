util = require('./lib/util.js');
tsurphu = require('./tsurphu-kongtrul.js');

epoch = {
  label: "Kongtrul epoch, 1852 (grub rtsis)",
  nyi: {
    nyidru: {
        add: util.radix([0, 1, 22, 2, 22], [1, 60, 60, 6, 67]), // nda1[]
        mul: util.radix([2, 10, 58, 1, 17], [1, 60, 60, 6, 67]) // nyidmg
    },
    nyilon: {
        mul: util.radix([0, 4, 21, 5, 43], [1, 60, 60, 6, 67]) // nyilmg
    },
    frac: 67
  }
}

module.exports = tsurphu.add(epoch);
