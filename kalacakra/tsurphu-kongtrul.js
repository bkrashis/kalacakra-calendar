util = require('./lib/util.js');

epoch = {
    label: "Kongtrul epoch, 1852.",
    zla: {
      year: 1852,
      zladag: {
          add: util.fraction(14,65) // 14 = ical_ind_b / eyr_a, EH "intercalation index"
      }
    },/*
    sun_grub: {
      nyidru: {
          add: util.radix([0, 1, 22, 2, 22], [1, 60, 60, 6, 67]) // nda1[]
      }
    },*/
    gza: {
        gzadru: {
            add: util.radix([2, 9, 24, 2, 304], [1, 60, 60, 6, 707]) // gda1[]
        },
    },
    nyi: {
      nyidru: {
          add: util.radix([0, 24, 15, 2, 4], [1, 60, 60, 6, 13]), // ndab[]
          mul: util.radix([2, 10, 58, 2, 10], [1, 60, 60, 6, 13]) // nyidmb
      },
      nyilon: {
          mul: util.radix([0, 4, 21, 5, 9], [1, 60, 60, 6, 13]) // nyilmb
      },
      frac: 13,
    },/*
    gza: {
      gzadru: {
          add: util.radix([1, 49, 40, 0, 0], [1, 60, 60, 6, 13]), // gdab[]
          mul: util.radix([1, 31, 50, 0, 0], [1, 60, 60, 6, 13]) // gzadmb
      },
      tsedru: {
          mul: util.radix([0, 59, 3, 4, 0], [1, 60, 60, 6, 13]) // tsedmb
      },
    },*/
    rilcha: {
        add: util.radix([0, 72], [1, 126])
    },
    zlasho: [],
    spizag: {
      b: 15, // spz_b
      c: 704, // spz_c
      f: 2, // spz_f
      j: 2397598 // spz_j
    },
    rahu: { part: 180 }, // rahupart, EH: 230 fraction for Rahu cycle
    planets: {
      mercury: { add: 3003 }, // meradd
      venus: { add: 686 }, // venadd
      mars: { add: 262 }, // maradd
      jupiter: { add: 2583 }, // jupadd
      saturn: { add: 437 }, // satadd
    },
    dragkang: {  // EH: This is added for "drag po'i rkang 'dzin".
      add: 83343 // dragkadd
    }
  };

module.exports = require('./lib/tradition.js')
                .add(require('./traditions/names.js'))
                .add(require('./traditions/events.js'))
                .add(require('./traditions/sadag.js'))
                .add(require('./traditions/tsurphu.js'))
                .add(epoch);
