util = require('./lib/util.js');

epoch = {
    label: "Tantra epoch, 806.",
    zla: {
      year: 806,
      zladag: {
          add: util.radix([0, 0], [1, 65]), // 0 = ical_ind_b / eyr_a, EH "intercalation index"
      }
    },/*
    sun: {
      nyidru: {
          add: util.radix([0, 29, 34, 5, 37], [1, 60, 60, 6, 67]) // nda2[]
      }
    },*/
    gza: {
        gzadru: {
            add: util.radix([2, 25, 20, 2, 352], [1, 60, 60, 6, 707]) // gda2[]
        }
    },
    nyi: {
      nyidru: {
          add: util.radix([26, 58, 0, 0, 0], [1, 60, 60, 6, 13]), // ndab2[]
          mul: util.radix([2, 10, 58, 2, 10], [1, 60, 60, 6, 13]) // nyidmb
      },
      nyilon: {
          mul: util.radix([0, 4, 21, 5, 9], [1, 60, 60, 6, 13]) // nyilmb
      },
      frac: 13,
    },/*
    gza: {
      gzadru: {
          add: util.radix([2, 30, 0, 0, 0], [1, 60, 60, 6, 13]), // gdab2[]
          mul: util.radix([1, 31, 50, 0, 0], [1, 60, 60, 6, 13]) // gzadmb
      },
      tsedru: {
          mul: util.radix([0, 59, 3, 4, 0], [1, 60, 60, 6, 13]) // tsedmb
      },
    },*/
    rilcha: {
        add: util.radix([5, 112], [1, 126])
    },
    zlasho: [],
    spizag: {
      b: 0, // spz_b
      c: 0, // spz_c
      f: 2, // spz_f
      j: 2015531 // spz_j
    },
    rahu: { part: 122 }, // rahupart, EH: 230 fraction for Rahu cycle
    planets: {
      mercury: { add: 1674 }, // meradd
      venus: { add: 2163 }, // venadd
      mars: { add: 167 }, // maradd
      jupiter: { add: 1732 }, // jupadd
      saturn: { add: 5946 }, // satadd
    },
    dragkang: {  // EH: This is added for "drag po'i rkang 'dzin".
      add: 179201 // dragkadd
    }
  };

module.exports = require('./lib/tradition.js')
                .add(require('./traditions/names.js'))
                .add(require('./traditions/events.js'))
                .add(require('./traditions/sadag.js'))
                .add(require('./traditions/tsurphu.js'))
                .add(epoch);
