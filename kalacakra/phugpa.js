util = require('./lib/util.js');

/* constants and overrides for Phugpa tradition */

epoch = {
    label: "Generalised Phugpa, -1000.",
    zla: {
      year: -1000,
      zladag: {
          add: util.fraction(7,65), // 7 = eyr_a, Henning's "intercalation index"
      }
    },
    nyi: {
        nyidru: {
            add: util.radix([26, 45, 53, 4, 26], [1, 60, 60, 6, 67]) // nda?[]
        }
    },
    gza: {
        gzadru: {
            add: util.radix([5, 35, 36, 4, 160], [1, 60, 60, 6, 707]) // gda?[]
        }
    },
    rilcha: {
        add: util.radix([6, 61], [1, 126]) // ril_a, ril_b
    },
    spizag: {
      b: 26, // spz_b
      c: 10, // spz_c
      f: 5, // spz_f
      j: 1355847 // spz_j
    },
    rahu: { part: 93 }, // rahupart, how far through the 230-month Rahu cycle
    planets: {
      mercury: { add: 2080 }, // meradd
      venus: { add: 277 }, // venadd
      mars: { add: 4 }, // maradd
      jupiter: { add: 511 }, // jupadd
      saturn: { add: 2995 }, // satadd
    },
    dragkang: {  // This is added for "drag po'i rkang 'dzin".
      add: 6663418 // dragkadd
    }
  };


module.exports = require('./lib/tradition.js')
                .add(require('./traditions/names.js'))
                .add(require('./traditions/events.js'))
                .add(require('./traditions/sadag.js'))
                .add(require('./traditions/names_byedpa_henning.js'))
                .add(epoch);
