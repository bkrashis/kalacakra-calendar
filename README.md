# kalacakra-calendar

A reimplementation in Javascript
of Edward Henning's algorithms for calculating
calendars in the Tibetan Kalacakra traditions.

The original code is available at http://kalacakra.org/

# Aims

* Interpreted code, compatible with browsers, calculations can run client side.
* Produce identical output to Henning's code for all supported traditions for at least years 2000-2499.
* Use the standard math.js library to make the code more readable. The original spends a lot of time dealing with representations of quantities in the traditional mixed-base format. This version treats quantities as high precision rationals using bignumber and fraction, converting to mixed-radix only for display.
* Abstract out tradition-specific quantities and code so that new traditions can be more easily added.
* More commenting.

# Current status

* Main calendar of Phugpa tradition fully supported except for planets.
* Output is identical for 2000-2499 for the detailed day-level output.
* For the calendar output, there are occasional rounding differences in "yoga"/"sbyorba" (in the last place of [60, 60, 6, 67]), to be investigated.
